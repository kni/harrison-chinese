jeet         = require 'jeet'
axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
css_pipeline = require 'css-pipeline'
rimraf       = require 'rimraf'


module.exports =
  ignores: ['bower.json','readme.md', '**/layout.*', '**/_*', '**/_*/*','.gitignore', 'ship.*conf']

  extensions: [
    js_pipeline
      manifest: 'assets/js/manifest.yml'
      out: 'js/build.js'
      # minify: true
    css_pipeline
      files: "assets/css/**"
      # minify: true
  ]

  stylus:
    import: ['jeet', 'rupture']
    use: [jeet(), axis(), rupture(), autoprefixer()]
    sourcemap: true

  'coffee-script':
    sourcemap: true

  jade:
    pretty: true

  server:
    clean_urls: true
    gzip: true
    error_page: '404.html'

  # Variables available to any page or script
  locals:
    pageTitle: '欢迎來到全新哈里森豪亭'
    siteAuthor: 'The Mark Company'
    ogTitle: '欢迎來到全新哈里森豪亭'
    ogType: 'website'
    ogUrl: 'http://devsitelocation.com/projects/harrison/china/'

  after: ->
    rimraf('public/bower_components', (err) ->
      if err
        console.warn err
    )
