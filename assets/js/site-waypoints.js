/*-------------------------------------
  EASY WAYPOINT FUNCTIONS
-------------------------------------*/
// Creates a standerd waypoint with the option of custom logic. To pass in
// the custom logic, just create a function with all the logic you would
// like to call when the waypoint is activated, then pass just the name of the
// function into this function without qoutes. Note that these waypoint functions are
// available to any js file in this project
// Example Single Waypoint: createWaypoint('.that', 'is-active', '35%', animateThat)
function createWaypoint(element, classToToggle, offset, cb) {
  return jQuery(element).waypoint(function(direction) {
    jQuery(element).toggleClass(classToToggle);
    if (typeof cb !== "undefined") {
      cb(element, classToToggle, offset, direction);
    }
  }, {
    offset: offset
  });
}

// A loop for standerd waypoint creation. Also has the ability to pass in custom
// logic, and classToToggle. Both are optional.
// Example Multiple Waypoints: waypointer(['.that', '#that', '#this'], 'resolved', '10%', animate);
function waypointer(elementArray, classToToggle, offset, cb) {
  for (var i = 0; i < elementArray.length; i++) {
    createWaypoint(elementArray[i], classToToggle, offset, cb);
  }
  return true;
}

function fullHeightOne() {
  $('#views > h2').toggleClass('active');
  $('#views > p').toggleClass('active');
}

function fullHeightTwo() {
  $('#amenities > .copy > h2').toggleClass('active');
  $('#amenities > .copy > p').toggleClass('active');
}

function fullHeightThree() {
  $('#residences > .copy > h2').toggleClass('active');
  $('#residences > .copy > p').toggleClass('active');
  $('#residences > .copy > a').toggleClass('active');
}

function fullHeightInterior() {
  $('#interior-headline').toggleClass('active');
  $('#interior-text').toggleClass('active');
  $('#interior-ul').toggleClass('active');
}

function fullHeightAmenities() {
  $('#amenities-headline').toggleClass('active');
  $('.amenities-text').toggleClass('active');
}

;
(function($) {
  $(function() {

    // place waypoints here
    createWaypoint('#views', null, '50%', fullHeightOne);
    createWaypoint('#amenities', null, '45%', fullHeightTwo);
    createWaypoint('#residences', null, '45%', fullHeightThree);

    createWaypoint('#amenitiestwo', null, '20%', fullHeightAmenities);
    // createWaypoint('#interior', null, '-20%', fullHeightInterior);

  });
}(jQuery));
