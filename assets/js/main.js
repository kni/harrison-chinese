var isHamOpen = false;

$(document).ready(function (){
  $(".mobile-nav-link").click(function (){
    console.log('in the zone')
    var navItem = $(this);
    if(isHamOpen) {
      $('body', 'html').removeClass('page-active')
      $('.overlay').removeClass('active');
      $('.mobile-nav').removeClass('active');
      $('body').addClass('no-transition');
      $('body').removeClass('nav-open');
      isHamOpen = false;

      setTimeout(function() {
        $('body').removeClass('no-transition');
      }, 1000)

    }else {
      $('body', 'html').addClass('page-active')
      $('body').addClass('nav-open');
      $('.mobile-nav').addClass('active');
      $('.overlay').addClass('active');
      isHamOpen = true;
    }
  });
});

;(function($) {
  var $root = $('html, body');
  $('a[href*=#]:not([href=#])').click(function() {
    if($(this).attr('class') != 'mobile-nav-link') {
      var href = $.attr(this, 'href');
      $root.animate({
        scrollTop: $(href).offset().top - 50
      }, 650, function() {
        window.location.hash = href;
      });
      return false;
    }
  });
}(jQuery));


$('.ham').click(function() {
  console.log('asf')
  if(isHamOpen) {
    $('body', 'html').removeClass('page-active')
    $('.overlay').removeClass('active');
    $('.mobile-nav').removeClass('active');
    $('body').removeClass('nav-open');
    isHamOpen = false;
  }else {
    $('body', 'html').addClass('page-active')
    $('body').addClass('nav-open');
    $('.mobile-nav').addClass('active');
    $('.overlay').addClass('active');
    isHamOpen = true;
  }
});

$('#mobile-close-btn').click(function() {
  if(isHamOpen) {
    $('body', 'html').removeClass('page-active')
    $('.overlay').removeClass('active');
    $('.mobile-nav').removeClass('active');
    $('body').removeClass('nav-open');
    isHamOpen = false;
  }else {
    $('body', 'html').addClass('page-active')
    $('body').addClass('nav-open');
    $('.mobile-nav').addClass('active');
    $('.overlay').addClass('active');
    isHamOpen = true;
  }
});

$('.overlay').click(function() {

  if(isHamOpen) {
    $('body', 'html').removeClass('page-active')
    $('.overlay').removeClass('active');
    $('.mobile-nav').removeClass('active');
    $('body').removeClass('nav-open');
    isHamOpen = false;
  }else if(isInquireOpen) {
    $('body', 'html').removeClass('page-active')
    $('.overlay').removeClass('active');
    $('.inquire-btn-menu').removeClass('active');
    $('body').removeClass('inquire-open');
    isInquireOpen = false;
  }
});
