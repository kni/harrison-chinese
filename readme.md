<img src="https://dl.dropboxusercontent.com/u/18590/radicle.png" width="150" height="auto"/>

_ˈradikəl/ n. – the part of a plant embryo that develops into the primary root._

Radicle is our static site boilerplate (which is also radical).

# Setup
- Make sure [node.js](http://nodejs.org) and [roots](http://roots.cx/articles/getting-started) are installed.
- Clone this repo down and `cd` into the folder.
- Run `npm install`. (which will automatically trigger `bower install`)
- Run `roots watch`. This will will perform all watch tasks, spin up a server, and point your browser to `localhost:1111/`

# Technologies / Dependencies
- **[Roots](http://roots.cx)** – Roots is a fast, simple, and customizable static site compiler and task runner. It is responsible for all minification, concatenation, and watch tasks for dev.
- **[Bower](http://bower.io/)** - Package manager for various front-end libraries
- **[Stylus](http://learnboost.github.io/stylus/)** – Stylus provides extremely fast, expressive, powerful, and robust pre-processing for our CSS. Also accepts Sass or vanilla CSS. (We usually try to stick to [these conventions](https://github.com/declandewet/stylus-conventions), however).
- **[Axis](http://axis.netlify.com)** – Axis is a robust css utility library
- **[Jeet](http://jeet.gs/)** – Flexible grid system to create layouts faster, with less code
- **[Rupture](http://jenius.github.io/rupture/)** – Stylus media query utility for clean breakpoints

# Deploying
- If you just want to compile the production build, run `roots compile -e production` and it will build to public.
- To deploy your site with a single command, run `roots deploy -to XXX` with `XXX` being whichever [ship](https://github.com/carrot/ship#usage) deployer you want to use.
